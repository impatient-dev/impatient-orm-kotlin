# impatient-orm-kotlin

An Object-Relational Mapper written in Kotlin.
This ORM converts between Kotlin objects and SQLite database records,
saving you from having to write tedious and error-prone code for every query.

This ORM also sets up the database schema, creating tables as necessary.
The database setup is capable of upgrading an existing database,
so it's fine to start with a basic schema and add more tables later.
The schema setup can also delete tables that are no longer needed, and add and remove columns.
It can also change existing columns, but there may be issues with some changes on some systems (Android vs JDBC).
Newly created tables will be in SQLite strict mode (if supported), but existing tables will be left non-strict.

SQLite is pretty case insensitive. Don't create things with the same name but different capitalization.

## Usage Instructions

### Declare Your Data Model

Write a Kotlin class to represent every table in your database. Example table with 3 columns and 1 index:

```kotlin
@Index("MyTable_amount", "amount")
data class MyTable (
	@PrimaryKey var id: Long,
	var name: String,
	var amount: Double?,
)
```

Supported data types:

* primitives (Boolean, Int, Long, Float, Double)
* String
* UUID

Other types (Instant, etc.) are not supported.

All member variables must be mutable and must appear in the constructor (Just write simple/normal data classes.)

### Set Up the ORM

```kotlin
val myOrm = Orm(listOf(MyTable::class))
```

### Set Up the Database Schema

This will create (and delete!) tables, etc. to match the set of tables you constructed the ORM with.
You don't have to do this if you'd rather set up your schema some other way.

```kotlin
fun doSetup(db: Database) {
	myOrm.applySchema(db)
}
```

### Use the ORM

```kotlin
fun doStuff(db: Database): Boolean {
	val table = myOrm.table(MyTable::class)
	val record = MyTable(-1, "foo", 123.45)
	table.save(record, db) //insert, since the primary key is -1; sets the primary key after inserting
	record.amount = null
	table.save(record, db) //update
	val loaded = Query(table).andWhere("name = ?").param("foo").selectFirst(db)
	return record == loaded //true
}
```

## Notes/Limitations

* Every table must have exactly 1 primary key, which must be an `Int` or `Long`. (or `Int?` or `Long?`)
This will store the SQLite rowid.

* The primary key must be mutable (`var`, not `val`), so the ORM can set it when you insert a new record.

* A primary key value of `-1` or `null` indicates that the record does not yet exist in the database.
Any other value (including `0`) represents a record that does exist in the database.

* The class must have a constructor that directly sets all properties. (The names and types all have to match.)
I recommend keeping it simple - don't write an `init` block or multiple constructors.

* Names (tables, columns, indexes, etc.) should consist of letters, numbers, and underscores only.
This ORM currently doesn't check whether your names are acceptable, and will not escape any unacceptable names.

* The schema setup can add and remove columns, but cannot change existing columns (e.g. changing the type).
This is an SQLite limitation.


## Future Improvements

* Add foreign key support