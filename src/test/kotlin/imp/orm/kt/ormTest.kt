//package imp.orm.kt
//import imp.sqlite.main.Sqlite
//import junit.framework.Assert.assertEquals
//import junit.framework.Assert.assertFalse
//import org.junit.Assert.assertNotSame
//import org.junit.Assert.assertTrue
//import org.junit.Test
//
//
//class TestBasicOrmOperations {
//	private data class Basic(
//		@PrimaryKey var id: Long,
//		var name: String?
//	)
//
//
//
//	private val orm = Orm(listOf(Basic::class))
//
//
//
//	@Test fun testCount() {
//		Sqlite.inMemory().use {db ->
//			orm.init(db)
//			val table = orm.table(Basic::class)
//			assertEquals(0, orm.query(table).count(db))
//
//			val first = Basic(-1, "A")
//			table.insert(first, db)
//			assertEquals(1, orm.query(table).count(db))
//			table.insert(Basic(-1, "B"), db)
//			assertEquals(2, orm.query(table).count(db))
//			table.delete(first, db)
//			assertEquals(1, orm.query(table).count(db))
//			table.delete(Basic(-123, "A"), db)
//			assertEquals(1, orm.query(table).count(db))
//		}
//	}
//
//
//	@Test fun testBasics() {
//		Sqlite.inMemory().use {db ->
//			orm.init(db)
//			val table = orm.table(Basic::class)
//			val a = Basic(-1, "A")
//			val b = Basic(-1, "B")
//
//			assertEquals(emptyList<Basic>(), orm.query(table).selectAll(db))
//			table.insert(a, db)
//			assertNotSame(-1, a.id)
//			assertEquals(listOf(a), orm.query(Basic::class).selectAll(db))
//
//			table.insert(b, db)
//			assertEquals(setOf(a,b), orm.query(table).selectAll(db).toSet())
//			assertTrue(setOf(a,b).contains(orm.query(table).selectFirst(db)))
//			assertEquals(2, orm.query(table).count(db))
//			assertEquals(listOf(a), orm.query(table).andWhere("name = ?").param("A").selectAll(db))
//
//			table.deletePk(a.id, db)
//			assertEquals(1, orm.query(Basic::class).count(db))
//			assertEquals(b, orm.query(Basic::class).selectOnly(db))
//		}
//	}
//
//
//	@Test fun testUpdate() {
//		Sqlite.inMemory().use {db ->
//			orm.init(db)
//			val table = orm.table(Basic::class)
//			val a = Basic(-1, "A")
//			val b = Basic(-1, "B")
//			table.insert(a, db)
//			table.insert(b, db)
//			assertFalse(a.id == b.id)
//
//			a.name = "Alpha"
//			table.update(a, db)
//			assertEquals(Basic(a.id, "Alpha"), orm.query(table).andWhere("id = ${a.id}").selectFirst(db))
//			assertEquals(Basic(b.id, "B"), orm.query(table).andWhere("id = ${b.id}").selectFirst(db))
//		}
//	}
//}