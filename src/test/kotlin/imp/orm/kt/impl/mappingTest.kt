package imp.orm.kt.impl

import imp.orm.kt.PrimaryKey
import imp.orm.kt.Table
import org.junit.Assert.assertEquals
import org.junit.Test
import kotlin.reflect.KClass

class Default {
	@PrimaryKey var id: Int? = null
	var name: String? = null

	override fun equals(that: Any?): Boolean {
		return that is Default && id == that.id && name == that.name
	}
}

data class Simple (
	@PrimaryKey var i: Int,
	var s: String
)


/**The ORM puts the primary key first, so it has to rearrange args before calling the constructor.*/
class Rearrange (
	var d: Double,
	var s: String,
	@PrimaryKey var i: Int
) {
	constructor(decoy: Any): this(0.0, 0, "0")

	constructor(d: Double, decoy2: Any, s: String): this(1.0, "1", 1)

	override fun equals(that: Any?): Boolean {
		return that is Rearrange && i == that.i && d == that.d && s == that.s
	}
}


class Unconstructible {
	@PrimaryKey var a: Int
	var b: Int

	constructor(first: Int, second: Int) {
		this.a = first
		this.b = second
	}
}


class WrongCtorType {
	@PrimaryKey var i: Int
	var s: String

	constructor(i: String, s: String) {
		this.i = 8
		this.s = s
	}
}


private fun <T: Any> getCreator(cls: KClass<T>): Creator<T> {
	return Table(cls, defaultTypeHandlers).factory
}


class MappingTest {
	@Test fun simple() {
		assertEquals(Simple(4, "four"), getCreator(Simple::class).create(arrayOf(4, "four")))
	}

	@Test fun default() {
		val expected = Default()
		expected.id = 5
		expected.name = "laptop"
		assertEquals(expected, getCreator(Default::class).create(arrayOf(5, "laptop")))
	}

	@Test fun rearrange() {
		val expected = Rearrange(-2.0, "negative", -3)
		val args: Array<Any?> = arrayOf(-3, -2.0, "negative") //primary key would come first; TODO just test using the actual ORM
		assertEquals(expected, getCreator(Rearrange::class).create(args))
	}

	@Test(expected = RuntimeException::class) fun unconstructible() {
		getCreator(Unconstructible::class)
	}

	@Test(expected = RuntimeException::class) fun wrongCtorType() {
		getCreator(WrongCtorType::class)
	}

}