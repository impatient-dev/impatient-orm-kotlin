package imp.orm.kt

import imp.orm.kt.impl.Sql
import imp.sqlite.Cursor
import imp.sqlite.Database

/**Finds records from a table that match conditions in a WHERE clause. You can select, count, or delete the records that match this query. Supports method chaining.*/
class Query<T: Any>(private val table: Table<T>) {
	/**Parts of the WHERE clause. AND will be inserted between them.*/
	private val whereTerms = ArrayList<String>()
	/**Prepared statement params in the WHERE clause.*/
	private val whereParams = ArrayList<Any?>()
	private val joins = ArrayList<String>()
	private val orderBy = StringBuilder()
	private var limit: Long? = null


	/**Appends a condition to the WHERE clause, AND'ing it with any previous conditions.
	 * The clause may contain "?" params, in which case each param must have a matching call to param().*/
	fun andWhere(sql: String): Query<T> {
		whereTerms.add(sql)
		return this
	}

	/**Sets a "?" param in the where clause. Calling this the nth time sets the nth param in the where clause.
	 * The value must be of a type that the database driver understands (primitives, string).*/
	fun param(value: Any?): Query<T> {
		whereParams.add(value)
		return this
	}

	/**Adds a join. This query will only ever return records from the original table (T),
	 * but adding joins allows you to use columns from other tables in your WHERE clause.
	 * The string should look something like "JOIN TableName ON foo = bar".
	 * (Previously this function required you to omit the JOIN keyword, but that has changed to allow outer joins.)
	 * (If your table name might be a SQLite keyword, put double quotes around the table name.)
	 * Joining a table to itself is not currently supported.
	 * Joins aren't allowed in a DELETE.*/
	fun join(str: String): Query<T> {
		joins.add(str)
		return this
	}


	/**Appends something to the order by clause, e.g. "name" or "date DESC".*/
	fun orderBy(sql: String): Query<T> {
		if(orderBy.isNotEmpty())
			orderBy.append(", ")
		orderBy.append(sql)
		return this
	}

	/**Limits the maximum number of results returned.*/
	fun limit(value: Long): Query<T> {
		limit = value
		return this
	}

	/**Limits the maximum number of results returned. Has no effect on count().*/
	fun limit(value: Int): Query<T> = limit(value.toLong())


	/**Undoes all calls to param(), so you can query by new params. You're responsible for setting the correct number of params.*/
	fun resetParams(): Query<T> {
		whereParams.clear()
		return this
	}


	/**Returns all records that match the query.*/
	fun selectAll(db: Database): ArrayList<T> = prepareAndQuery(db) { cursor ->
		val out = ArrayList<T>()
		while(cursor.moveToNext())
			out.add(table.fromCurrentRecord(cursor))
		out
	}

	/**Sends records that match the query to a lambda, one at a time.*/
	fun selectEach(db: Database, action: (T) -> Unit) = prepareAndQuery(db) { cursor ->
		while(cursor.moveToNext())
			action(table.fromCurrentRecord(cursor))
	}

	/**Returns the first result that matches the query, and ignores any other results.
	 * Returns null if there are no results.*/
	fun selectFirst(db: Database): T? = prepareAndQuery(db, limit = 1) { cursor ->
		if(cursor.moveToNext()) table.fromCurrentRecord(cursor) else null
	}

	/**Returns the only record that matches the query. Throws an error if multiple records match.*/
	fun selectOnly(db: Database): T? = prepareAndQuery(db, limit = 2) { cursor ->
		if(!cursor.moveToNext())
			return null
		val out = table.fromCurrentRecord(cursor)
		if(!cursor.moveToNext()) out else throw RuntimeException("Multiple records matched the query.")
	}


	/**Prepares the SQL and sets any parameters.*/
	private inline fun <R> prepareAndQuery(db: Database, limit: Long? = this.limit, action: (Cursor) -> R): R {
		val sql = Sql.select(table, joinSafe = !joins.isEmpty(), newline = true)
		Sql.joins(joins, sql, true)
		Sql.where(whereTerms, sql, true)
		Sql.orderBy(orderBy.toString(), sql, true)
		if(limit != null)
			Sql.limit(limit, sql, true)

		db.prepareSelect(sql.toString()).use { stmt ->
			whereParams.forEachIndexed { i, param -> stmt.setParam(i, param) }
			stmt.select().use { return action(it) }
		}
	}


	/**Returns the number of records that match the query. Any limit you previously provided is ignored.*/
	fun count(db: Database): Long {
		val sql = Sql.count(table)
		Sql.joins(joins, sql, true)
		Sql.where(whereTerms, sql, true)

		db.prepareSelect(sql.toString()).use { stmt ->
			whereParams.forEachIndexed { i, param -> stmt.setParam(i, param) }
			stmt.select().use { cursor ->
				cursor.moveToNext()
				return cursor.reqLong(0)
			}
		}
	}


	/**Deletes all records that match the query.
	 * This is not allowed if there are joins.*/
	fun delete(db: Database) {
		if(!joins.isEmpty())
			throw IllegalStateException("Cannot use joins in a DELETE")
		val sql = Sql.delete(table)
		Sql.where(whereTerms, sql, true)
		db.prepareUpdate(sql.toString()).use { stmt ->
			whereParams.forEachIndexed { i, param -> stmt.setParam(i, param) }
			stmt.update()
		}
	}
}