package imp.orm.kt

import imp.orm.kt.impl.SchemaHandler
import imp.orm.kt.impl.SqliteSchema
import imp.orm.kt.impl.TypeHandler
import imp.orm.kt.impl.defaultTypeHandlers
import imp.sqlite.Database
import imp.util.logger
import kotlin.reflect.KClass

private val log = Orm::class.logger

/**An Object-Relational Mapper, which converts between Kotlin code and a SQLite database.*/
class Orm(
	tables: Collection<KClass<out Any>>
) {
	private val tablesByClass: Map<KClass<*>, Table<out Any>>
	private val types: Map<KClass<out Any>, TypeHandler<out Any>> = defaultTypeHandlers

	init {
		val map = HashMap<KClass<*>, Table<out Any>>()
		for(cls in tables) {
			map[cls] = Table(cls, types)
		}
		this.tablesByClass = map
	}

	/**Adds/removes tables, columns, etc. so the database has exactly what this ORM requires.
	 * The database can be empty, or may already contain tables, records, indexes, etc.*/
	fun applySchema(db: Database) {
		log.debug("Setting up schema.")
		val current = SchemaHandler.read(db)
		val desired = SchemaHandler.from(tablesByClass.values)
		SchemaHandler.apply(current, desired, db)
		log.debug("Schema setup successfully.")
	}

	val schema: SqliteSchema get() = SchemaHandler.from(tablesByClass.values)


	fun <T: Any> table(cls: KClass<T>): Table<T> {
		val out: Table<*>? = tablesByClass[cls]
		return if(out != null) out as Table<T> else throw IllegalArgumentException("No table found for $cls")
	}

	fun <T: Any> query(cls: KClass<T>): Query<T> = Query(table(cls))
	fun <T: Any> query(table: Table<T>): Query<T> = Query(table)
}