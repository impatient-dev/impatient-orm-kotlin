package imp.orm.kt.impl

/**Describes the schema (tables, indexes, etc.) of a Sqlite database.*/
data class SqliteSchema (
	val tablesByName: Map<String, SqliteTable>,
	val indexes: Map<String, SqliteIndex>,
)

data class SqliteTable (
	val name: String,
	val columns: Map<String, SqliteColumn>,
)

data class ColRef (
	val table: String,
	val column: String,
)

data class SqliteColumn (
	val name: String,
	val type: SqliteTypeAffinity,
	val notNull: Boolean,
	val primaryKey: Boolean,
	val foreignKeyTo: ColRef?,
)

enum class SqliteTypeAffinity {
	TEXT,
	NUMERIC,
	INTEGER,
	REAL,
	BLOB,
}

data class SqliteForeignKey (
	val referencingTable: String,
	val referencingColumn: String,
	val referencedTable: String,
	val referencedColumn: String,
)

data class SqliteIndex (
	val name: String,
	val tableName: String,
	val unique: Boolean,
	val columns: List<SqliteIndexCol>,
) {
	fun equalsIgnoreCase(that: SqliteIndex): Boolean {
		if(
			!name.equals(that.name, true) ||
			!tableName.equals(that.tableName, true) ||
			unique != that.unique ||
			columns.size != that.columns.size
		) return false
		for(i in 0 until columns.size)
			if(!columns[i].equalsIgnoreCase(that.columns[i]))
				return false
		return true
	}
}

data class SqliteIndexCol (
	val name: String,
	val ascending: Boolean,
) {
	fun equalsIgnoreCase(that: SqliteIndexCol): Boolean {
		return ascending == that.ascending && name.equals(that.name, true)
	}
}