package imp.orm.kt.impl

import imp.orm.kt.Table


/**Writes SQL statements. The statements do not end with semicolons, and do not begin or end with whitespace.*/
object Sql {
	fun createTable(table: SqliteTable, strict: Boolean, out: StringBuilder = StringBuilder()): StringBuilder {
		out.append("CREATE TABLE \"")
		out.append(table.name)
		out.append("\" (")
		table.columns.values.forEachIndexed {i, col ->
			if(i > 0)
				out.append(", ")
			columnDef(col, out)
		}
		out.append(')')
		if(strict)
			out.append(" STRICT")
		return out
	}

	fun addColumn(tableName: String, column: SqliteColumn, out: StringBuilder = StringBuilder()): StringBuilder {
		out.append("ALTER TABLE \"")
		out.append(tableName)
		out.append("\" ADD COLUMN ")
		columnDef(column, out)
		return out
	}

	private fun columnDef(column: SqliteColumn, out: StringBuilder): StringBuilder {
		out.append('"')
		out.append(column.name)
		out.append("\" ")
		out.append(column.type.name)
		if(column.primaryKey)
			out.append(" PRIMARY KEY")
		if(column.notNull)
			out.append(" NOT NULL")
		if(column.foreignKeyTo != null) {
			out.append(" REFERENCES \"")
			out.append(column.foreignKeyTo.table)
			out.append("\"(\"")
			out.append(column.foreignKeyTo.column)
			out.append("\")")
		}
		return out
	}

	fun renameColumn(table: String, old: String, new: String, out: StringBuilder = StringBuilder()): StringBuilder {
		out.append("ALTER TABLE \"")
		out.append(table)
		out.append("\" RENAME COLUMN \"")
		out.append(old)
		out.append("\" TO \"")
		out.append(new)
		out.append('"')
		return out
	}

	fun dropColumn(table: String, column: String, out: StringBuilder = StringBuilder()): StringBuilder {
		out.append("ALTER TABLE \"")
		out.append(table)
		out.append("\" DROP COLUMN \"")
		out.append(column)
		out.append('"')
		return out
	}

	fun createIndex(index: SqliteIndex, out: StringBuilder = StringBuilder()): StringBuilder {
		out.append("CREATE")
		if(index.unique)
			out.append(" UNIQUE")
		out.append(" INDEX \"")
		out.append(index.name)
		out.append("\" ON \"")
		out.append(index.tableName)
		out.append("\" (")
		index.columns.forEachIndexed { i, col ->
			if(i > 0)
				out.append(",")
			out.append('"')
			out.append(col.name)
			out.append('"')
		}
		out.append(")")
		return out
	}


	/**Writes an INSERT statement using "?" params. Leaves the primary key out, so it can be generated.*/
	fun fullInsert(table: Table<*>, out: StringBuilder = StringBuilder()): StringBuilder {
		out.append("INSERT INTO \"")
		out.append(table.name)
		out.append("\" (")
		table.otherColumns.forEachIndexed { i, col ->
			if (i > 0)
				out.append(", ")
			out.append('"')
			out.append(col.name)
			out.append('"')
		}
		out.append(") VALUES (")
		(0 until table.otherColumns.size).forEach {
			if (it > 0)
				out.append(',')
			out.append('?')
		}
		out.append(')')
		return out
	}


	/**Writes an UPDATE statement using "?" params. All columns except the primary key are to be updated.*/
	fun fullUpdate(table: Table<*>, out: StringBuilder = StringBuilder()): StringBuilder {
		out.append("UPDATE \"")
		out.append(table.name)
		out.append("\" SET ")
		table.otherColumns.forEachIndexed { i, col ->
			if (i > 0)
				out.append(", ")
			out.append('"')
			out.append(col.name)
			out.append("\" = ?")
		}
		return out
	}


	/**Writes the basics of a DELETE statement.*/
	fun delete(table: Table<*>, out: StringBuilder = StringBuilder()): StringBuilder {
		out.append("DELETE FROM \"")
		out.append(table.name)
		out.append('"')
		return out
	}


	/**Writes a SELECT statement that selects records from a table.
	 * If this statement may be used with JOINs, set joinSafe to true and we'll add the table name to all columns to disambiguate them.*/
	fun select(table: Table<*>, out: StringBuilder = StringBuilder(), joinSafe: Boolean = false, newline: Boolean = false): StringBuilder {
		out.append("SELECT ")
		if(joinSafe) {
			out.append('"')
			out.append(table.name)
			out.append("\".")
		}
		out.append('"')
		out.append(table.primaryKey.name)
		out.append('"')
		for (col in table.otherColumns) {
			out.append(", ")
			if(joinSafe) {
				out.append('"')
				out.append(table.name)
				out.append("\".")
			}
			out.append('"')
			out.append(col.name)
			out.append('"')
		}

		separator(out, newline)
		out.append("FROM \"")
		out.append(table.name)
		out.append('"')
		return out
	}


	/**Writes a statement to SELECT the number of rows in a table.*/
	fun count(table: Table<*>, out: StringBuilder = StringBuilder()): StringBuilder {
		out.append("SELECT COUNT(*) FROM \"")
		out.append(table.name)
		out.append('"')
		return out
	}


	/**Writes joins, if any. Each item should look something like "JOIN TableName ON foo = bar".*/
	fun joins(joins: List<String>, out: StringBuilder = StringBuilder(), newline: Boolean = false): StringBuilder {
		for(join in joins) {
			separator(out, newline)
			out.append(join)
		}
		return out
	}


	/**Writes a WHERE clause consisting of 0 or more terms AND'ed together.
	 * @param newline if anything is written, a newline will be the first thing*/
	fun where(andTerms: Collection<String>, out: StringBuilder = StringBuilder(), newline: Boolean = false): StringBuilder {
		if(andTerms.isEmpty())
			return out
		separator(out, newline)
		andTerms.forEachIndexed { i, term ->
			out.append(if (i == 0) "WHERE " else " AND ")
			out.append(term)
		}
		return out
	}


	/**Writes a SQL LIMIT clause to limit the number of records returned by a SELECT statement.
	 * @param newline if true, the clause will start with a newline*/
	fun limit(limit: Long, out: StringBuilder = StringBuilder(), newline: Boolean = false): StringBuilder {
		separator(out, newline)
		out.append("LIMIT ")
		out.append(limit)
		return out
	}

	fun orderBy(cols: String, out: StringBuilder, newline: Boolean = false): StringBuilder {
		if(cols.isNotEmpty()) {
			separator(out, newline)
			out.append("ORDER BY ")
			out.append(cols)
		}
		return out
	}

	/**Appends either a newline or a space.*/
	private fun separator(out: StringBuilder, newline: Boolean) = if(newline) out.appendLine() else out.append(' ')
}