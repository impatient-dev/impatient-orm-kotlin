package imp.orm.kt.impl

import imp.orm.kt.Orm
import imp.orm.kt.OrmIndex
import imp.orm.kt.Table
import imp.sqlite.Database
import imp.sqlite.exec
import imp.sqlite.selectEach
import imp.sqlite.selectOnlyLong
import imp.sqlite.selectOnlyString
import imp.sqlite.withTransaction
import imp.sqlite.withoutForeignKeys
import imp.util.exCtx
import imp.util.logger
import imp.util.putNew
import java.lang.Integer.parseInt

private val log = SchemaHandler::class.logger

object SchemaHandler {
	/**Discovers the current schema present in this database.*/
	fun read(db: Database): SqliteSchema {
		log.trace("Reading database schema.")
		val tableNames = ArrayList<String>()
		db.selectEach("SELECT name FROM sqlite_master WHERE type = 'table'") { cursor ->
			tableNames.add(cursor.reqString(0))
		}

		val tables = HashMap<String, SqliteTable>(tableNames.size)
		val indexes = HashMap<String, SqliteIndex>()

		for(tableName in tableNames) {
			log.trace("Reading table {}.", tableName)
			val columns = HashMap<String, SqliteColumn>()
			db.selectEach("PRAGMA table_info(\"$tableName\")") { cursor ->
				val fksByCol = HashMap<String, SqliteForeignKey>()

				db.selectEach("PRAGMA foreign_key_list(\"$tableName\")") { cursor ->
					val toTable = cursor.reqString("table")
					val fromCol = cursor.reqString("from")
					val toCol = cursor.reqString("to")
					fksByCol.putNew(fromCol, SqliteForeignKey(
						referencingTable = tableName, referencingColumn = fromCol,
						referencedTable = cursor.reqString("table"), referencedColumn = cursor.reqString("to")))
				}

				val colName = cursor.reqString("name")
				val col = SqliteColumn(
					name = colName,
					type = SqliteTypeAffinity.valueOf(cursor.reqString("type")),
					notNull = cursor.reqBoolean("notnull"),
					primaryKey = cursor.reqBoolean("pk"),
					foreignKeyTo = fksByCol[colName]?.let { ColRef(it.referencedTable, it.referencedColumn) })
				columns[col.name] = col
			}
			tables[tableName] = SqliteTable(tableName, columns)

			db.selectEach("PRAGMA index_list(\"$tableName\")") { cursor ->
				val indexName = cursor.reqString("name")
				log.trace("Reading index {}.", indexName)
				val indexUnique = cursor.reqBoolean("unique")
				val indexCols = ArrayList<SqliteIndexCol>()
				db.selectEach("PRAGMA index_xinfo(\"$indexName\")") { indexCursor ->
					val idxColName = indexCursor.getString("name")
					if(idxColName != null)
						indexCols.add(SqliteIndexCol(idxColName, !indexCursor.reqBoolean("desc")))
				}
				indexes.putNew(indexName, SqliteIndex(indexName, tableName, indexUnique, indexCols)) {"Duplicate index $indexName in tables ${it.tableName} and $tableName."}
			}
		}

		return SqliteSchema(tables, indexes)
	}


	/**Parses a schema from some ORM tables.*/
	fun from(tables: Collection<Table<*>>): SqliteSchema {
		val inTables = tables.associateBy { it.name }
		val outTables = HashMap<String, SqliteTable>()
		for(t in tables) {
			val columns = HashMap<String, SqliteColumn>(t.allColumns.size)
			for(c in t.allColumns) {
				val fkTarget = if(c.referencesTable == null) null else {
					val toTable = inTables[c.referencesTable] ?: throw IllegalArgumentException("Foreign key ${t.name}.${c.name} references nonexistent table ${c.referencesTable}")
					ColRef(toTable.name, toTable.primaryKey.name)
				}
				columns[c.name] = SqliteColumn(c.name, c.typeHandler.affinity, !c.optional, c.primaryKey, fkTarget)
			}
			outTables[t.name] = SqliteTable(t.name, columns)
		}

		val indexes = HashMap<String, SqliteIndex>()
		for(table in tables)
			for(index in table.indexes)
				indexes.compute(index.name) {_, conflict ->
					if(conflict != null)
						throw RuntimeException("Duplicate index ${index.name} in tables ${index.tableName} and ${conflict.tableName}")
					index
				}

		return SqliteSchema(outTables, indexes)
	}


	fun indexFrom(annotation: OrmIndex, tableName: String): SqliteIndex {
		try {
			val cols = ArrayList<SqliteIndexCol>()
			for(part in annotation.columns.split(", ")) {
				val halves = part.split(' ')
				if(halves.size > 2 || halves.size == 0 || (halves.size == 2 && halves[1].uppercase() != "ASC" && halves[1].uppercase() != "DESC"))
					throw RuntimeException("Invalid format for columns: \"${annotation.columns}\"")
				cols.add(SqliteIndexCol(halves[0], halves.size < 2 || halves[1].uppercase() == "ASC"))
			}
			return SqliteIndex(annotation.name, tableName, annotation.unique, cols)
		} catch(e: Exception) {
			throw RuntimeException("Failed to parse index ${annotation.name} for table $tableName", e)
		}
	}


	/**Adds and removes tables, columns, etc. to change the database from the current schema to the desired one.
	 * Warning: this function can delete existing data.*/
	fun apply(desired: Orm, db: Database) = apply(read(db), desired.schema, db)

	/**Adds and removes tables, columns, etc. to change the database from the current schema to the desired one.
	 * This function does not check that the current schema actually matches the state of the database.
	 * Warning: this function can delete existing data.*/
	fun apply(current: SqliteSchema, desired: SqliteSchema, db: Database) {
		val strict = supportsStrict(db)
		// on Android, cannot (dis)able foreign keys in a transaction
		db.withoutForeignKeys {
			db.withTransaction {
				removeIndexes(current, desired, db)
				removeColumns(current, desired, db)
				removeTables(current, desired, db)
				alterColumns(current, desired, db)
				addTables(current, desired, strict, db)
				addColumns(current, desired, db)
				addIndexes(current, desired, db)
			}
		}
	}
}


/**Removes indexes that should not exist, and indexes that will need to be recreated.*/
private fun removeIndexes(current: SqliteSchema, desired: SqliteSchema, db: Database) {
	current.indexes.forEach { name, index ->
		val new = desired.indexes[name]
		if(new == null || !new.equalsIgnoreCase(index)) {
			log.debug("Dropping index {}.", name)
			db.exec("DROP INDEX \"$name\"")
		}
	}
}

/**Create indexes that are new, or that had to be recreated.*/
private fun addIndexes(current: SqliteSchema, desired: SqliteSchema, db: Database) {
	desired.indexes.forEach { name, index ->
		val old = current.indexes[name]
		if(old == null || !old.equalsIgnoreCase(index)) {
			log.debug("Creating index {}.", name)
			db.exec(Sql.createIndex(index).toString())
		}
	}
}

/**Removes tables that should not exist.*/
private fun removeTables(current: SqliteSchema, desired: SqliteSchema, db: Database) {
	current.tablesByName.forEach { name, _ ->
		if(!desired.tablesByName.containsKey(name)) {
			log.debug("Dropping table {}.", name)
			db.exec("DROP TABLE \"$name\"")
		}
	}
}

/**Adds new tables.*/
private fun addTables(current: SqliteSchema, desired: SqliteSchema, strict: Boolean, db: Database) {
	desired.tablesByName.forEach { name, table ->
		if(!current.tablesByName.containsKey(name)) {
			log.debug("Creating table {}.", name)
			db.exec(Sql.createTable(table, strict).toString())
		}
	}
}

/**Removes columns that should not exist.*/
private fun removeColumns(current: SqliteSchema, desired: SqliteSchema, db: Database) {
	for(t0 in current.tablesByName.values) {
		val t1 = desired.tablesByName[t0.name] ?: continue
		t0.columns.forEach { name, col ->
			if(!t1.columns.containsKey(name)) {
				log.debug("Dropping column {}.{}", t0.name, name)
				db.exec(Sql.dropColumn(t0.name, name).toString())
			}
		}
	}
}

/**Adds new columns that don't currently exist.*/
private fun addColumns(current: SqliteSchema, desired: SqliteSchema, db: Database) {
	for(t1 in desired.tablesByName.values) {
		val t0 = current.tablesByName[t1.name] ?: continue
		t1.columns.forEach { name, col ->
			if(!t0.columns.containsKey(name)) {
				log.debug("Creating column {}.{}", t1.name, name)
				db.exec(Sql.addColumn(t1.name, col).toString())
			}
		}
	}
}

/**Change columns that exist in both current and desired.*/
private fun alterColumns(current: SqliteSchema, desired: SqliteSchema, db: Database) {
	for (t0 in current.tablesByName.values) {
		val t1 = desired.tablesByName[t0.name] ?: continue
		t1.columns.forEach { name, c1 ->
			val c0 = t0.columns[name]
			if (c0 != null && c0 != c1)
				alterColumn(t0, t1, c0, c1, db)
		}
	}
}

private fun alterColumn(t0: SqliteTable, t1: SqliteTable, c0: SqliteColumn, c1: SqliteColumn, db: Database) {
	log.debug("Altering column {}.{}.", t0.name, c0.name)
	if(c0.primaryKey || c1.primaryKey)
		throw IllegalArgumentException("Cannot alter primary key ${t0.name}.${c0.name}")
	if(!c0.notNull && c1.notNull) {
		val count = db.selectOnlyLong("SELECT COUNT(*) FROM \"${t0.name}\" WHERE \"${c0.name}\" IS NULL")
		if(count != 0L)
			throw IllegalStateException("Cannot alter ${t0.name}.${c0.name} to be non-null, because there are $count existing records with null values.")
	}

	val tempColName = "${c0.name}_old"
	db.exec(Sql.renameColumn(t0.name, c0.name, tempColName).toString())
	db.exec(Sql.addColumn(t0.name, c1).toString())
	db.exec("UPDATE \"${t0.name}\" SET \"${c1.name}\" = \"$tempColName\"")
	db.exec(Sql.dropColumn(t0.name, tempColName).toString())
}


/**Checks that the database version is 3.37.0 or higher.
 * Actually, for now this just returns false, because my db viewer doesn't support strict tables.*/
private fun supportsStrict(db: Database): Boolean {
	if(true)
		return false
	try {
		val str = db.selectOnlyString("SELECT sqlite_version()")!!
		exCtx({ "Unexpected version format: $str"}) {
			val parts = str.split(".").map(::parseInt)
			check(parts.size >= 2)
			return parts[0] > 3 || (parts[0] == 3 && parts[1] >= 37)
		}
	} catch(e: Exception) {
		log.error("Unable to determine SQLite version.", e)
		return false
	}
}