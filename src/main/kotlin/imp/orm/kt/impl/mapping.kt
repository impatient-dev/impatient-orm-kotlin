package imp.orm.kt.impl

import imp.orm.kt.Table
import kotlin.reflect.KClass
import kotlin.reflect.KFunction
import kotlin.reflect.jvm.isAccessible


/**Creates an object and fills all its columns. The arguments in the array must be in the same order as the columns were+are.
 * This interface exists to support both classes that take everything as a constructor param,
 * and classes with 0-arg constructors where we set all the (mutable) properties after calling the constructor.*/
interface Creator<T> {
	fun create(args: Array<Any?>): T
}


/**Creates an empty object using a 0-arg constructor, then sets each column.*/
private class TwoStepCreator<T: Any>: Creator<T> {
	private val columns: List<Table<T>.Column>
	private val ctor: KFunction<T>

	constructor(columns: List<Table<T>.Column>, factory: KFunction<T>) {
		this.columns = columns
		this.ctor = factory
	}

	override fun create(args: Array<Any?>): T {
		val out = ctor.call()
		columns.forEachIndexed {i, col ->
			if(args[i] != null)
				col.toRecord(args[i]!!, out)
		}
		return out
	}
}


/**Calls a multi-arg constructor with a matching array of arguments.
 * Rearranges the arguments before calling the constructor, to match the order of the constructor args.*/
private class RearrangingCreator<T: Any>: Creator<T> {
	private val indexMapping: Array<Int>
	private val factory: KFunction<T>

	/**@Param indexMapping maps the indices of input arguments to the indices where they should be provided to the constructor.
	 * The first int is the index of the constructor argument the first input value is for.*/
	constructor(indexMapping: Array<Int>, ctor: KFunction<T>) {
		this.indexMapping = indexMapping
		this.factory = ctor
		factory.isAccessible = true
	}

	override fun create(args: Array<Any?>): T {
		val rearrangedArgs = Array<Any?>(indexMapping.size) { null }
		for(i in 0 until args.size)
			rearrangedArgs[indexMapping[i]] = args[i]
		return factory.call(*rearrangedArgs)//spread operator
	}
}


/**Generates a Creator that can create instances of the provided class from values for the provided columns in the order they appear here.*/
fun <T: Any> matchingCreator(cls: KClass<T>, columns: List<Table<T>.Column>): Creator<T>? {
	//see if we can use a 2-step creator first
	val defaultCtor = cls.constructors.find {it.parameters.isEmpty()}
	if(defaultCtor != null)
		return TwoStepCreator(columns, defaultCtor)

	//prepare a mapping of column name -> input index
	val columnIndexesByName = HashMap<String, Int>()
	columns.forEachIndexed {colIndex, col ->
		columnIndexesByName[col.name] = colIndex
	}

	//see if any constructor can be used to automatically construct the object
	for(ctor in cls.constructors) {
		if(ctor.parameters.size != columns.size)
			continue

		val matchingConstructorParams = Array(columns.size) {-1}
		var someArgsUnknown = false

		ctor.parameters.forEachIndexed {ctorIndex, ctorParam ->
			val name = ctorParam.name
			if(name == null || columnIndexesByName[name] == null || ctorParam.type != columns[columnIndexesByName[name]!!].type)
				someArgsUnknown = true
			else
				matchingConstructorParams[columnIndexesByName[name]!!] = ctorIndex
		}

		if(!someArgsUnknown) {
			return RearrangingCreator(matchingConstructorParams, ctor)
		}
	}

	return null
}