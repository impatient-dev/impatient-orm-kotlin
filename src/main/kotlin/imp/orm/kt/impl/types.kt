package imp.orm.kt.impl

import imp.sqlite.Cursor
import imp.sqlite.PreparedStatement
import java.nio.ByteBuffer
import java.util.*
import kotlin.reflect.KClass


val defaultTypeHandlers: Map<KClass<out Any>, TypeHandler<out Any>> = mapOf(
	Pair(Boolean::class, BoolHandler),
	Pair(Short::class, ShortHandler),
	Pair(Int::class, IntHandler),
	Pair(Long::class, LongHandler),
	Pair(Float::class, FloatHandler),
	Pair(Double::class, DoubleHandler),
	Pair(String::class, StringHandler),
	Pair(UUID::class, UuidHandler),
	Pair(ByteArray::class, ByteArrayHandler),
)




/**Handles a type that may be used as a value for a database column.*/
interface TypeHandler<T> {
	val affinity: SqliteTypeAffinity

	/**Returns whether a value is of the type this TypeHandler handles.*/
	fun isOfType(value: Any): Boolean
	/**Tries to convert a value to something this type can handle. Throws an error if this type cannot accept the value.*/
	fun tryConvert(value: Any): T

	/**Extracts a value of this type from a Cursor.*/
	fun from(cursor: Cursor, column: Int): T?
	/**Sets a value of this type as a parameter in a prepared statement.*/
	fun toParam(value: T, col: Int, stmt: PreparedStatement)
}


object BoolHandler: TypeHandler<Boolean> {
	override val affinity = SqliteTypeAffinity.INTEGER
	override fun isOfType(value: Any) = value is Boolean
	override fun tryConvert(value: Any) = value as? Boolean ?: throw IllegalArgumentException("Cannot convert $value to boolean.")

	override fun from(cursor: Cursor, column: Int) = cursor.getBoolean(column)

	override fun toParam(value: Boolean, col: Int, stmt: PreparedStatement) {
		stmt.setBoolean(col, value)
	}
}


object ShortHandler: TypeHandler<Short> {
	override val affinity = SqliteTypeAffinity.INTEGER
	override fun isOfType(value: Any) = value is Short
	override fun tryConvert(value: Any): Short {
		return when (value) {
			is Short -> value
			else -> throw IllegalArgumentException("Cannot convert $value to integer.")
		}
	}

	override fun from(cursor: Cursor, column: Int) = cursor.getShort(column)

	override fun toParam(value: Short, col: Int, stmt: PreparedStatement) {
		stmt.setShort(col, value)
	}
}


object IntHandler: TypeHandler<Int> {
	override val affinity = SqliteTypeAffinity.INTEGER
	override fun isOfType(value: Any) = value is Int
	override fun tryConvert(value: Any): Int {
		return when (value) {
			is Int -> value
			is Long ->
				if(value >= Int.MIN_VALUE.toLong() && value <= Int.MAX_VALUE) value.toInt()
				else throw IllegalArgumentException("Int $value is too large.")
			else -> throw IllegalArgumentException("Cannot convert $value to integer.")
		}
	}

	override fun from(cursor: Cursor, column: Int) = cursor.getInt(column)

	override fun toParam(value: Int, col: Int, stmt: PreparedStatement) {
		stmt.setInt(col, value)
	}
}


object LongHandler: TypeHandler<Long> {
	override val affinity = SqliteTypeAffinity.INTEGER
	override fun isOfType(value: Any) = value is Long
	override fun tryConvert(value: Any): Long = when (value) {
		is Int -> value.toLong()
		is Long -> value
		else -> throw IllegalArgumentException("Cannot convert $value to long.")
	}

	override fun from(cursor: Cursor, column: Int) = cursor.getLong(column)

	override fun toParam(value: Long, col: Int, stmt: PreparedStatement) {
		stmt.setLong(col, value)
	}
}


object FloatHandler: TypeHandler<Float> {
	override val affinity = SqliteTypeAffinity.REAL
	override fun isOfType(value: Any) = value is Float
	override fun tryConvert(value: Any): Float = when (value) {
		is Float -> value
		is Double -> value.toFloat()
		else -> throw IllegalArgumentException("Cannot convert $value to float.")
	}

	override fun from(cursor: Cursor, column: Int) = cursor.getFloat(column)

	override fun toParam(value: Float, col: Int, stmt: PreparedStatement) {
		stmt.setFloat(col, value)
	}
}


object DoubleHandler: TypeHandler<Double> {
	override val affinity = SqliteTypeAffinity.REAL
	override fun isOfType(value: Any) = value is Double
	override fun tryConvert(value: Any): Double = when (value) {
		is Float -> value.toDouble()
		is Double -> value
		else -> throw IllegalArgumentException("Cannot convert $value to double.")
	}

	override fun from(cursor: Cursor, column: Int) = cursor.getDouble(column)

	override fun toParam(value: Double, col: Int, stmt: PreparedStatement) {
		stmt.setDouble(col, value)
	}
}


object StringHandler: TypeHandler<String> {
	override val affinity = SqliteTypeAffinity.TEXT
	override fun isOfType(value: Any) = value is String
	override fun tryConvert(value: Any) = value as? String ?: throw IllegalArgumentException("Cannot convert $value to string.")

	override fun from(cursor: Cursor, column: Int) = cursor.getString(column)

	override fun toParam(value: String, col: Int, stmt: PreparedStatement) {
		stmt.setString(col, value)
	}
}


object UuidHandler: TypeHandler<UUID> {
	override val affinity = SqliteTypeAffinity.BLOB
	override fun isOfType(value: Any) = value is UUID
	override fun tryConvert(value: Any): UUID = when(value) {
		is UUID -> value
		is String -> UUID.fromString(value)
		else -> throw IllegalArgumentException("Cannot convert $value to UUID.")
	}

	override fun from(cursor: Cursor, column: Int): UUID? {
		val bytes = cursor.getBlob(column)
		if(bytes == null)
			return null
		check(bytes.size == 16) { "A UUID can't be ${bytes.size} bytes." }
		val wrap = ByteBuffer.wrap(bytes)
		return UUID(wrap.long, wrap.long)
	}

	override fun toParam(value: UUID, col: Int, stmt: PreparedStatement) {
		val bytes = ByteArray(16)
		val wrap = ByteBuffer.wrap(bytes)
		wrap.putLong(value.mostSignificantBits)
		wrap.putLong(value.leastSignificantBits)
		stmt.setBlob(col, bytes)
	}
}


object ByteArrayHandler: TypeHandler<ByteArray> {
	override val affinity = SqliteTypeAffinity.BLOB
	override fun isOfType(value: Any) = value is ByteArray
	override fun tryConvert(value: Any): ByteArray = when(value) {
		is ByteArray -> value
		else -> throw IllegalArgumentException("Cannot convert $value to a byte array")
	}

	override fun from(cursor: Cursor, column: Int) = cursor.getBlob(column)
	override fun toParam(value: ByteArray, col: Int, stmt: PreparedStatement) = stmt.setBlob(col, value)
}