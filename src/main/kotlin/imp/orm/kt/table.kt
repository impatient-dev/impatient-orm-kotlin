package imp.orm.kt

import imp.orm.kt.impl.*
import imp.sqlite.Cursor
import imp.sqlite.Database
import imp.sqlite.PreparedStatement
import imp.util.logger
import kotlin.reflect.KClass
import kotlin.reflect.KMutableProperty1
import kotlin.reflect.KType
import kotlin.reflect.full.findAnnotation
import kotlin.reflect.full.memberProperties
import kotlin.reflect.jvm.isAccessible

private val log = Table::class.logger

/**Our representation of a database table.*/
class Table<T: Any> (
	cls: KClass<T>,
	types: Map<KClass<out Any>, TypeHandler<out Any>>,
) {
	val name: String
	/**Creates instances of the class using the default constructor.*/
	val factory: Creator<T>
	val primaryKey: Column
	/**Columns other than the primary key.*/
	val otherColumns: List<Column>
	val allColumns: List<Column>
	val indexes: List<SqliteIndex>


	init {
		val tableAnnotation = cls.findAnnotation<OrmTable>()
		this.name = tableAnnotation?.name ?: cls.simpleName ?: throw IllegalArgumentException("No name available for class $cls")
		var pk: Column? = null
		val columns = ArrayList<Column>()

		for(prop in cls.memberProperties) {
			if(prop !is KMutableProperty1) {
				log.warn("Property $name.${prop.name} is not mutable and will be ignored.")
				continue
			}
			val col = Column(prop as KMutableProperty1<T, Any>, types)
			if(col.primaryKey) {
				if(pk != null)
					throw IllegalArgumentException("$cls has multiple primary keys: $pk and $prop.")
				pk = col
			} else {
				columns.add(col)
			}
		}

		this.primaryKey = pk ?: throw IllegalArgumentException("$cls is missing a primary key.")
		this.otherColumns = columns
		this.allColumns = listOf(primaryKey).plus(otherColumns)

		this.factory = matchingCreator(cls, this.allColumns) ?: throw IllegalArgumentException("Unsure how to construct instances of $cls.")

		val indexes = ArrayList<SqliteIndex>()
		for(annotation in cls.annotations) {
			if(annotation !is OrmIndex)
				continue
			indexes.add(SchemaHandler.indexFrom(annotation, name))
		}
		this.indexes = indexes
	}


	fun inserter(db: Database) = RecordInserter(this, db)
	fun query() = Query(this)

	fun requireColumn(colName: String): Column {
		return otherColumns.find { it.name == colName } ?: throw RuntimeException("No such column '$colName' in table $name")
	}


	/**For each column in the list, gets the value from the record and sets it in the prepared statement. The first column is param 0, etc.*/
	private fun setCols(columns: List<Column>, record: T, stmt: PreparedStatement) {
		columns.forEachIndexed {i, col ->
			val colValue = col.fromRecord(record)
			if(colValue == null)
				stmt.setNull(i)
			else
				(col.typeHandler as TypeHandler<Any>).toParam(colValue, i, stmt)
		}
	}


	/**Inserts the record, and updates its primary key with whatever the database generated. (The current value of the primary key is ignored and overwritten.)*/
	fun insert(record: T, db: Database) {
		RecordInserter(this, db).use {
			it.insert(record)
		}
	}

	fun update(record: T, db: Database) {
		RecordUpdater(this, db).use {
			it.update(record)
		}
	}

	/**Inserts the record if it is missing its primary key, or updates it otherwise.*/
	fun save(record: T, db: Database) {
		val pk = primaryKey.fromRecord(record)
		if(pk == null || pk == -1 || pk == -1L)
			insert(record, db)
		else
			update(record, db)
	}

	fun delete(record: T, db: Database) {
		val pkValue = primaryKey.fromRecord(record)
		if(pkValue == null)
			throw IllegalArgumentException("Cannot delete by a null primary key from $record.")
		else
			deletePk(pkValue, db)
	}

	/**Deletes a record by its primary key.*/
	fun deletePk(pk: Any, db: Database) {
		assert(primaryKey.typeHandler.isOfType(pk)) {"$pk is not a valid value for the primary key $primaryKey of table $name"}
		db.prepareUpdate("DELETE FROM $name WHERE ${primaryKey.name} = ?").use {stmt ->
			(primaryKey.typeHandler as TypeHandler<Any>).toParam(pk, 0, stmt)
			stmt.update()
		}
	}


	/**Creates an object from the record the cursor is looking at.
	 * The columns in the SQL used to form the cursor must match the order in Table.allColumns.*/
	fun fromCurrentRecord(cursor: Cursor): T {
		val values = Array(allColumns.size) { i ->
			allColumns[i].typeHandler.from(cursor, i)
		}
		return factory.create(values)
	}



	inner class Column {
		val name: String
		private val field: KMutableProperty1<T, Any>
		val type: KType
		val typeHandler: TypeHandler<out Any>
		/**Whether null is allowed.*/
		val optional: Boolean
		val primaryKey: Boolean
		/**If this column is a foreign key, this is the table it references. (The column is always the primary key.*/
		val referencesTable: String?

		constructor(field: KMutableProperty1<T, Any>, types: Map<KClass<out Any>, TypeHandler<out Any>>) {
			this.name = field.name
			this.field = field
			this.type = field.returnType
			this.typeHandler = types[type.classifier!! as KClass<Any>] ?: throw RuntimeException("Unsure how to handle field $field of type ${field.returnType}")
			this.primaryKey = field.findAnnotation<PrimaryKey>() != null
			this.optional = field.returnType.isMarkedNullable
			referencesTable = field.findAnnotation<References>()?.tableName
			field.isAccessible = true

			if(primaryKey && typeHandler !is IntHandler && typeHandler !is LongHandler)
				throw IllegalArgumentException("Primary key $field must be either an Int or a Long")

		}


		fun toRecord(value: Any, record: T) {
			field.set(record, value)
		}

		fun fromRecord(record: T): Any? {
			return field.get(record)
		}

		override fun toString() = "Column[$name ${typeHandler.javaClass.simpleName}${if(optional) "?" else ""} ${if(primaryKey) "PK" else ""}]"
	}
}