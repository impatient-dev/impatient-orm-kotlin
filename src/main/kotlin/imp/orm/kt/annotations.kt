package imp.orm.kt

import kotlin.annotation.AnnotationRetention.RUNTIME
import kotlin.annotation.AnnotationTarget.*

/**Lets you set the name of a table. If this is missing, the class name will be used.*/
@Target(allowedTargets = [CLASS])
@Retention(RUNTIME)
annotation class OrmTable (
	val name: String
)

/**Marks a property as a table's primary key. The field must be an Int or Long, and may be optional (Long?).
 * The field must be modifiable (though the setter may be private).
 * For a primary key, the value -1 is treated the same as the value null.*/
@Target(allowedTargets = [FIELD, PROPERTY])
@Retention(RUNTIME)
annotation class PrimaryKey

/**Creates a foreign key relationship, indicating that this property refers to the primary key of some table.
 * The property must be Int or Long, and null is allowed. The invalid ID (-1) is not allowed.
 * A non-null value must correspond to the ID of some record that exists in the database.*/
@Target(allowedTargets = [FIELD, PROPERTY])
@Retention(RUNTIME)
annotation class References (
	val tableName: String
)

@Target(allowedTargets = [CLASS])
@Retention(RUNTIME)
@Repeatable
annotation class OrmIndex (
	/**Must be unique among all tables and indexes.*/
	val name: String,
	/**E.g. "foo, bar DESC, baz ASC". The separator must be ", " (comma+space); different numbers of spaces are not acceptable.*/
	val columns: String,
	val unique: Boolean = false,
)