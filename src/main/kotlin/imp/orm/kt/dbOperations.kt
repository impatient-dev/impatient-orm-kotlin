package imp.orm.kt

import imp.orm.kt.impl.Sql
import imp.orm.kt.impl.TypeHandler
import imp.sqlite.Database
import imp.sqlite.PreparedInsert
import imp.sqlite.PreparedStatement
import imp.sqlite.PreparedUpdate


/**For each column in the list, gets the value from the record and sets it in the prepared statement. The first column is param 0, etc.*/
private fun <T: Any> setCols(columns: List<Table<T>.Column>, record: T, stmt: PreparedStatement) {
	columns.forEachIndexed {i, col ->
		val colValue = col.fromRecord(record)
		if(colValue == null)
			stmt.setNull(i)
		else
			(col.typeHandler as TypeHandler<in Any>).toParam(colValue, i, stmt)
	}
}



/**Inserts records for a table into the database, one at a time.*/
class RecordInserter <T: Any> (val table: Table<T>, db: Database) : AutoCloseable {
	private val stmt: PreparedInsert = db.prepareInsert(Sql.fullInsert(table).toString())

	/**Inserts the record and updates its primary key. The existing primary key value is ignored.*/
	fun insert(record: T) {
		setCols(table.otherColumns, record, stmt)
		var pk = stmt.insert()
		pk = table.primaryKey.typeHandler.tryConvert(pk)
		table.primaryKey.toRecord(pk, record)
	}

	override fun close() = stmt.close()
}


/**Updates records of a table, one at a time.*/
class RecordUpdater <T: Any> (val table: Table<T>, db: Database) : AutoCloseable {
	private val stmt: PreparedUpdate

	init {
		val sql = Sql.fullUpdate(table)
		sql.append("\nWHERE ${table.primaryKey.name} = ?")
		stmt = db.prepareUpdate(sql.toString())
	}

	/**Updates the database record to match this one. The argument must have a primary key.*/
	fun update(record: T) {
		setCols(table.otherColumns, record, stmt)
		stmt.setParam(table.otherColumns.size, table.primaryKey.fromRecord(record))
		stmt.update()
	}

	override fun close() = stmt.close()
}