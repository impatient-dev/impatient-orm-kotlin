package imp.orm.kt

import imp.orm.kt.impl.Sql
import imp.sqlite.Cursor
import imp.sqlite.Database

/**A query that returns columns from multiple tables, or specific columns from one table.
 * This query variant doesn't provide any ORM features; you have to write a rowMapper for your query, reading columns by name or index.
 * (If you want all columns from 1 table, use Query<T> instead.)
 * Supports method chaining, and select and count (but not delete).*/
class GeneralQuery (
	/**The name of the table to select from.*/
	private val from: String,
	/**Some columns to select (more can be added later).
	 * If a column name might be a SQLite keyword, you should surround it with double quotes.
	 * You can provide multiple column names, separated by commas.*/
	vararg columns: String
) {
	private val columns = StringBuilder()
	/**Parts of the WHERE clause. AND will be inserted between them.*/
	private val whereTerms = ArrayList<String>()
	private val whereParams = ArrayList<Any?>()
	private val joins = ArrayList<String>()
	private val orderBy = StringBuilder()
	private var limit: Long? = null

	init {
		columns.forEachIndexed { i, col ->
			if(i > 0)
				this.columns.append(", ")
			this.columns.append(col)
		}
	}


	/**Adds a column name to the list of columns we're selecting. "colName" or "TableName.colName".
	 * You can provide multiple column names, separated by commas.
	 * If a table or column name might be a SQLite keyword, you should surround it with double quotes.*/
	fun col(str: String): GeneralQuery {
		if(columns.isNotEmpty())
			columns.append(", ")
		columns.append(str)
		return this
	}

	/**Appends a condition to the WHERE clause, AND'ing it with any previous conditions.
	 * The clause may contain "?" params, in which case each param must have a matching call to param().*/
	fun andWhere(sql: String): GeneralQuery {
		whereTerms.add(sql)
		return this
	}

	/**Sets a "?" param in the WHERE clause. Calling this the nth time sets the nth param in the where clause.
	 * The value must be of a type that the database driver understands (primitives, string).*/
	fun param(value: Any?): GeneralQuery {
		whereParams.add(value)
		return this
	}

	/**Adds a join. The string should look something like "JOIN TableName ON foo = bar".
	 * If your table name might be a SQLite keyword, put double quotes around the table name.
	 * Table name aliases are allowed.*/
	fun join(str: String): GeneralQuery {
		joins.add(str)
		return this
	}

	/**Appends something to the order by clause, e.g. "name" or "date DESC".*/
	fun orderBy(sql: String): GeneralQuery {
		if(orderBy.isNotEmpty())
			orderBy.append(", ")
		orderBy.append(sql)
		return this
	}

	/**Limits the maximum number of results returned. Has no effect on count().*/
	fun limit(value: Long): GeneralQuery {
		limit = value
		return this
	}

	/**Undoes all calls to param(), so you can query by new params. You're responsible for setting the correct number of params.*/
	fun resetParams(): GeneralQuery {
		whereParams.clear()
		return this
	}


	/**Sends records that match the query to a lambda, one at a time.*/
	fun selectEach(db: Database, action: (Cursor) -> Unit) = prepareAndQuery(db) { cursor ->
		while(cursor.moveToNext())
			action(cursor)
	}

	/**Returns all records that match the query.*/
	fun <T> selectAll(db: Database, rowMapper: (Cursor) -> T): ArrayList<T> = prepareAndQuery(db) { cursor ->
		val out = ArrayList<T>()
		while(cursor.moveToNext())
			out.add(rowMapper(cursor))
		out
	}

	/**Returns the first result that matches the query, and ignores any other results.
	 * Returns null if nothing matches the query.*/
	fun <T> selectFirst(db: Database, rowMapper: (Cursor) -> T): T? = prepareAndQuery(db, limit = 1) { cursor ->
		if(cursor.moveToNext()) rowMapper(cursor) else null
	}

	/**Returns the only record that matches the query.
	 * Returns null if no records match the query; throws an error if multiple records match.*/
	fun <T> selectOnly(db: Database, rowMapper: (Cursor) -> T): T? = prepareAndQuery(db, limit = 2) { cursor ->
		if(!cursor.moveToNext())
			return null
		val out = rowMapper(cursor)
		if(!cursor.moveToNext()) out else throw RuntimeException("Multiple records matched the query.")
	}


	/**Prepares the SQL and sets any parameters.*/
	private inline fun <R> prepareAndQuery(db: Database, limit: Long? = this.limit, action: (Cursor) -> R): R {
		check(columns.isNotEmpty()) { "No columns specified" }
		val sql = StringBuilder("SELECT ")
		sql.append(columns)
		sql.append("\nFROM ")
		sql.append(from)
		Sql.joins(joins, sql, true)
		Sql.where(whereTerms, sql, true)
		Sql.orderBy(orderBy.toString(), sql, true)
		if(limit != null)
			Sql.limit(limit, sql, true)

		db.prepareSelect(sql.toString()).use { stmt ->
			whereParams.forEachIndexed { i, param -> stmt.setParam(i, param) }
			stmt.select().use { return action(it) }
		}
	}


	/**Returns the number of records that match the query. Any limit you previously provided is ignored.*/
	fun count(db: Database): Long {
		val sql = StringBuilder("SELECT COUNT(*) FROM ")
		sql.append(from)
		Sql.joins(joins, sql, true)
		Sql.where(whereTerms, sql, true)

		db.prepareSelect(sql.toString()).use { stmt ->
			whereParams.forEachIndexed { i, param -> stmt.setParam(i, param) }
			stmt.select().use { cursor ->
				cursor.moveToNext()
				return cursor.reqLong(0)
			}
		}
	}
}